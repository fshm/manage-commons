Commons as we know is a political idea where both natural and artificial resources, both material and immaterial, both tangible and non-tangible resources are held in common for everyone's usage.

The commons belongs to everyone. At the same time, we also know about a paradigm called **tragedy of the commons**, a condition where the resource treated as commons is spoiled by those who wanted it to be commons. It was the late professor, **Elinor Ostrom**, who listed out a number of guidelines to successfully manage a commons, after researching various communities across the world.

Listing out the [8 principles for successfully Managing commons](http://www.onthecommons.org/magazine/elinor-ostroms-8-principles-managing-commmons).

1. Define Clear Group Boundaries.
2. Match rules governing use of common goods to local needs and conditions.
3. Ensure that those affected by the rules can participate in modifying the rules.
4. Make sure the rule-making rights of community members are respected by outside authorities.
5. Develop a system, carried out by community members, for monitoring members’ behavior.
6. Use graduated sanctions for rule violators.
7. Provide accessible, low-cost means for dispute resolution.
8. Build responsibility for governing the common resource in nested tiers from the lowest level up to the entire interconnected system.


Let's see if we can express those guidelines in programming languages and build a tool to speak for managing commons.

### Entities Involved

1. Commons Resource - self explanatory.
2. Groups - forming groups are natural for humans. There might be multiple groups who want to access a resource held as commons, for example. a physical space for conducting various activities.
3. Members - here comes the commoners belonging to various groups.
4. Events - any activity that revolves around the commons. For example, if there is one telescope and one of the group wants to access it to organize a night sky watch. Here the intention and the purpose of utilizing the resource is termed as an event.
5. Incidents - incidents are any actions that causes damage to the commons resource. For example, leaving a commons space unlocked or uncleaned assuming wrong information.

### Technical Implementation

Server

1. RESTful API

Client

1. Apps for phones.
2. Website.

and what if, this can also be federated, so groups can run their own instances and be able to advertise and discover new commons to and from other such groups and utilize those?

### Licensing

1. Server part can be in AGPL v3 as of now.
2. Client part can be in GPL v3 as of now.
